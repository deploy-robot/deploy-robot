use super::machine::RobotMachine;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct RobotRelease {
    name: String,
    version: u32,
    machines: Vec<RobotMachine>,
}

impl RobotRelease {
    pub fn new<S>(name: S, version: u32, machines: Vec<RobotMachine>) -> Self
    where
        S: Into<String>,
    {
        RobotRelease {
            name: name.into(),
            version,
            machines,
        }
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn version(&self) -> u32 {
        self.version
    }

    pub fn machines(&self) -> Vec<RobotMachine> {
        self.machines.clone()
    }

    pub fn add_machine(&mut self, machine: RobotMachine) {
        self.machines.push(machine);
    }
}
