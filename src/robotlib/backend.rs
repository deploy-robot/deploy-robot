use super::machine::RobotMachineState;
use super::RobotDomain;
use super::{machine::RobotMachine, providers, RobotError, RobotOs, RobotPlan};
use async_std::task;
use digitalocean_rs::DigitalOceanApi;
use hcloud::apis::servers_api::DeleteServerParams;
use hcloud::apis::servers_api::ShutdownServerParams;
use linode_rs::LinodeApi;
use providers::hetzner;
use scaleway_rs::ScalewayApi;
use std::time::Duration;
use upcloud_rs::UpcloudApi;
use vultr::VultrApi;

#[derive(Clone)]
pub enum RobotBackend {
    DigitalOcean(DigitalOceanApi),
    Linode(LinodeApi),
    Vultr(VultrApi),
    Hetzner(hcloud::apis::configuration::Configuration),
    Upcloud(UpcloudApi),
    ScaleWay(ScalewayApi),
}

impl RobotBackend {
    pub async fn list_os(&self) -> Result<Vec<RobotOs>, RobotError> {
        match self {
            RobotBackend::DigitalOcean(api) => providers::digitalocean::list_os(api).await,
            RobotBackend::Linode(api) => providers::linode::list_os(api).await,
            RobotBackend::Vultr(api) => providers::vultr::list_os(api).await,
            RobotBackend::Hetzner(config) => hetzner::list_os(config).await,
            RobotBackend::Upcloud(api) => providers::upcloud::list_os(api).await,
            RobotBackend::ScaleWay(config) => providers::scaleway::list_os(config).await,
        }
    }

    pub async fn list_machines(&self) -> Result<Vec<RobotMachine>, RobotError> {
        match self {
            RobotBackend::DigitalOcean(api) => providers::digitalocean::list_machines(api).await,
            RobotBackend::Linode(api) => providers::linode::list_machines(api).await,
            RobotBackend::Vultr(api) => providers::vultr::list_machines(api).await,
            RobotBackend::Hetzner(config) => hetzner::list_machines(config).await,
            RobotBackend::Upcloud(api) => providers::upcloud::list_machines(api).await,
            RobotBackend::ScaleWay(config) => providers::scaleway::list_machines(config).await,
        }
    }

    pub async fn get_machine(&self, machine: &RobotMachine) -> Result<RobotMachine, RobotError> {
        match self {
            RobotBackend::DigitalOcean(api) => {
                providers::digitalocean::get_machine(api, machine.id()).await
            }
            RobotBackend::Linode(api) => {
                providers::linode::get_machine(api, machine.id().parse().unwrap()).await
            }
            RobotBackend::Vultr(api) => providers::vultr::get_machine(api, machine.id()).await,
            RobotBackend::Hetzner(config) => hetzner::get_machine(config, machine.id()).await,
            RobotBackend::Upcloud(api) => providers::upcloud::get_machine(api, machine.id()).await,
            RobotBackend::ScaleWay(api) => {
                providers::scaleway::get_machine(api, machine.location(), machine.id()).await
            }
        }
    }

    pub async fn list_plans(&self) -> Result<Vec<RobotPlan>, RobotError> {
        match self {
            RobotBackend::DigitalOcean(api) => providers::digitalocean::list_plans(api).await,
            RobotBackend::Linode(api) => providers::linode::list_plans(api).await,
            RobotBackend::Vultr(api) => providers::vultr::list_plans(api).await,
            RobotBackend::Hetzner(config) => hetzner::list_plans(config).await,
            RobotBackend::Upcloud(api) => providers::upcloud::list_plans(api).await,
            RobotBackend::ScaleWay(config) => providers::scaleway::list_plans(config).await,
        }
    }

    pub async fn create_machine<S1, S2>(
        &self,
        plan: &RobotPlan,
        release_name: S1,
        release_version: u32,
        image: S2,
        container_port: u32,
        vultr_os_id: u32,
        hetzner_os_id: u32,
        upcloud_os_id: String,
        digitalocean_os_id: String,
        linode_os_id: String,
        scaleway_marketplace_os_id: String,
        scaleway_default_project_id: Option<String>,
    ) -> Result<RobotMachine, RobotError>
    where
        S1: AsRef<str>,
        S2: AsRef<str>,
    {
        let user_data = format!(
            r##"#cloud-config
packages:
- docker.io
runcmd:
- systemctl start docker
- docker pull nginx
- docker run -d --name {release} --restart=always -p 80:{port} {image}"##,
            release = release_name.as_ref(),
            image = image.as_ref(),
            port = container_port,
        );

        use base64::{engine::general_purpose, Engine as _};
        let user_data_encoded: String = general_purpose::STANDARD.encode(user_data.clone());

        match self {
            RobotBackend::DigitalOcean(api) => {
                providers::digitalocean::create_machine(
                    api,
                    release_name,
                    release_version,
                    user_data,
                    digitalocean_os_id,
                    plan,
                )
                .await
            }
            RobotBackend::Linode(api) => {
                providers::linode::create_machine(
                    api,
                    release_name,
                    release_version,
                    user_data_encoded,
                    linode_os_id,
                    plan,
                )
                .await
            }
            RobotBackend::Vultr(api) => {
                providers::vultr::create_machine(
                    api,
                    release_name,
                    release_version,
                    user_data_encoded,
                    vultr_os_id,
                    plan,
                )
                .await
            }
            RobotBackend::Hetzner(config) => {
                providers::hetzner::create_machine(
                    config,
                    release_name,
                    release_version,
                    user_data_encoded,
                    hetzner_os_id,
                    plan,
                )
                .await
            }
            RobotBackend::Upcloud(api) => {
                providers::upcloud::create_machine(
                    api,
                    release_name,
                    release_version,
                    user_data_encoded,
                    upcloud_os_id,
                    plan,
                )
                .await
            }
            RobotBackend::ScaleWay(api) => {
                providers::scaleway::create_machine(
                    api,
                    release_name,
                    release_version,
                    user_data,
                    scaleway_marketplace_os_id,
                    scaleway_default_project_id,
                    plan,
                )
                .await
            }
        }
    }

    pub async fn delete_release<S>(
        &self,
        release_name: S,
        release_version: u32,
    ) -> Result<(), RobotError>
    where
        S: AsRef<str>,
    {
        let machines = self.list_machines().await?;
        for machine in machines {
            if machine.release_name().eq(release_name.as_ref())
                && machine.release_version().eq(&release_version)
            {
                self.delete_machine(machine).await?;
            }
        }
        Ok(())
    }

    /*
    pub async fn delete_releases<S>(&self, release_name: S) -> Result<(), RobotError>
    where
        S: AsRef<str>,
    {
        let machines = self.list_machines().await?;
        for machine in machines {
            if machine.release_name().eq(release_name.as_ref()) {
                self.delete_machine(machine).await?;
            }
        }
        Ok(())
    }
    */

    pub async fn delete_machine(&self, machine: RobotMachine) -> Result<(), RobotError> {
        if self.delete_needs_stop_first() {
            self.stop_instance_wait_async(&machine).await?;
        }
        match self {
            RobotBackend::DigitalOcean(api) => api.delete_droplet_async(machine.id()).await?,
            RobotBackend::Linode(api) => {
                api.delete_instance_async(machine.id().parse().unwrap())
                    .await?
            }
            RobotBackend::Vultr(api) => {
                api.delete_instance_async(machine.id()).await?;
            }
            RobotBackend::Hetzner(config) => {
                hcloud::apis::servers_api::delete_server(
                    config,
                    DeleteServerParams {
                        id: machine.id().parse().unwrap(),
                    },
                )
                .await?;
            }
            RobotBackend::Upcloud(api) => {
                api.delete_instance_async(machine.id()).await?;
            }
            RobotBackend::ScaleWay(api) => {
                providers::scaleway::delete_instance(api, &machine).await?;
            }
        }
        Ok(())
    }

    pub fn delete_needs_stop_first(&self) -> bool {
        match self {
            RobotBackend::DigitalOcean(_) => false,
            RobotBackend::Linode(_) => false,
            RobotBackend::Vultr(_) => false,
            RobotBackend::Hetzner(_) => false,
            RobotBackend::Upcloud(_) => true,
            RobotBackend::ScaleWay(_) => true,
        }
    }

    pub async fn stop_instance_async(&self, machine: &RobotMachine) -> Result<(), RobotError> {
        match self {
            RobotBackend::DigitalOcean(api) => {
                api.initiate_droplet_action_async(machine.id(), "power_off")
                    .await?;
            }
            RobotBackend::Linode(api) => {
                api.shutdown_instance_async(machine.id().parse().unwrap())
                    .await?;
            }
            RobotBackend::Vultr(api) => {
                api.stop_instance_async([machine.id()]).await?;
            }
            RobotBackend::Hetzner(config) => {
                let params = ShutdownServerParams {
                    id: machine.id().parse().unwrap(),
                };
                hcloud::apis::servers_api::shutdown_server(config, params).await?;
            }
            RobotBackend::Upcloud(api) => {
                api.stop_instance_async(machine.id()).await?;
            }
            RobotBackend::ScaleWay(api) => {
                api.perform_instance_action_async(&machine.location(), machine.id(), "poweroff")
                    .await?;
            }
        };
        Ok(())
    }

    pub async fn stop_instance_wait_async(&self, machine: &RobotMachine) -> Result<(), RobotError> {
        loop {
            let m = self.get_machine(&machine).await?;
            println!("Machine state: {:?}", m.state());
            match m.state() {
                RobotMachineState::Stopped => break,
                RobotMachineState::Started => self.stop_instance_async(machine).await?,
                RobotMachineState::Error => self.stop_instance_async(machine).await?,
                RobotMachineState::Transitioning => {}
            }
            task::sleep(Duration::from_secs(1)).await;
        }
        Ok(())
    }

    pub async fn list_dns(&self) -> Result<(), RobotError> {
        match self {
            RobotBackend::Vultr(api) => {
                let domains = api.get_dns_domain_list_async().await?;
                println!("{:#?}", domains);

                for domain in domains {
                    let entries = api.get_dns_domain_records_async(domain.domain).await?;
                    println!("{:#?}", entries);
                }
            }
            _ => {
                // no dns support
            }
        };
        Ok(())
    }

    pub async fn get_dns_domain<S1: AsRef<str>>(
        &self,
        domainname: S1,
    ) -> Result<Vec<RobotDomain>, RobotError> {
        let mut list = vec![];
        match self {
            RobotBackend::Vultr(api) => {
                list.push(providers::vultr::domain2robot(
                    self.clone(),
                    providers::vultr::get_dns_domain(api, domainname).await?,
                ));
            }
            _ => {}
        }
        Ok(list)
    }

    pub async fn delete_dns_name<S1: AsRef<str>>(&self, hostname: S1) -> Result<(), RobotError> {
        if let Some((entryname, domainname)) = hostname.as_ref().split_once(".") {
            match self {
                RobotBackend::Vultr(api) => {
                    providers::vultr::delete_dns_name(api, domainname, entryname).await
                }
                _ => {
                    // no dns support
                    Ok(())
                }
            }
        } else {
            Ok(())
        }
    }
}
