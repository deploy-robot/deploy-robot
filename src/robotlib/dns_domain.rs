use super::{
    backend::RobotBackend, providers, robotlib::ProviderType, RobotDomainEntry, RobotError,
};

pub struct RobotDomain {
    pub api: RobotBackend,
    pub provider: ProviderType,
    pub name: String,
}

impl RobotDomain {
    pub async fn create_record<S1: AsRef<str>, S2: AsRef<str>, S3: AsRef<str>>(
        &self,
        record_type: S1,
        entryname: S2,
        data: S3,
        ttl: u32,
    ) -> Result<(), RobotError> {
        match &self.api {
            RobotBackend::Vultr(api) => {
                api.create_dns_domain_record_async(
                    &self.name,
                    record_type.as_ref(),
                    entryname.as_ref(),
                    data.as_ref(),
                    Some(ttl),
                    None,
                )
                .await?;
            }
            _ => {}
        };
        Ok(())
    }

    pub async fn get_records(&self) -> Result<Vec<RobotDomainEntry>, RobotError> {
        match &self.api {
            RobotBackend::Vultr(api) => {
                let records = api.get_dns_domain_records_async(&self.name).await?;
                Ok(records
                    .into_iter()
                    .map(|item| {
                        providers::vultr::domainentry2robot(self.api.clone(), &self.name, item)
                    })
                    .collect())
            }
            _ => Err(RobotError::DNSNotSupportedByProvider),
        }
    }
}
