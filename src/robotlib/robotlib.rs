use std::collections::HashMap;

use digitalocean_rs::DigitalOceanApi;
use linode_rs::LinodeApi;
use log::error;
use scaleway_rs::ScalewayApi;
use upcloud_rs::UpcloudApi;
use vultr::VultrApi;

use super::{
    backend::RobotBackend, machine::RobotMachine, release::RobotRelease, RobotDomain, RobotError,
    RobotOs, RobotPlan,
};

pub struct RobotLib {
    list: HashMap<ProviderType, RobotBackend>,
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub enum ProviderType {
    DigitalOcean,
    Linode,
    Vultr,
    Hetzner,
    Upcloud,
    ScaleWay,
}

impl std::fmt::Display for ProviderType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ProviderType::DigitalOcean => write!(f, "DigitalOcean"),
            ProviderType::Linode => write!(f, "Linode"),
            ProviderType::Vultr => write!(f, "Vultr"),
            ProviderType::Hetzner => write!(f, "Hetzner"),
            ProviderType::Upcloud => write!(f, "Upcloud"),
            ProviderType::ScaleWay => write!(f, "ScaleWay"),
        }
    }
}

impl RobotLib {
    pub fn new() -> Self {
        RobotLib {
            list: HashMap::new(),
        }
    }

    pub fn add_digitalocean(&mut self, digitalocean_api_key: &str) {
        let api = DigitalOceanApi::new(digitalocean_api_key);
        self.list
            .insert(ProviderType::DigitalOcean, RobotBackend::DigitalOcean(api));
    }

    pub fn add_linode(&mut self, linode_api_key: &str) {
        let api = LinodeApi::new(linode_api_key);
        self.list
            .insert(ProviderType::Linode, RobotBackend::Linode(api));
    }

    pub fn add_vultr(&mut self, vultr_api_key: &str) {
        let api = VultrApi::new(vultr_api_key);
        self.list
            .insert(ProviderType::Vultr, RobotBackend::Vultr(api));
    }

    pub fn add_hetzner(&mut self, key: &str) {
        use hcloud::apis::configuration::Configuration;
        let mut configuration = Configuration::new();
        configuration.bearer_access_token = Some(key.to_string());
        self.list
            .insert(ProviderType::Hetzner, RobotBackend::Hetzner(configuration));
    }

    pub fn add_upcloud(&mut self, username: &str, password: &str) {
        let api = UpcloudApi::new(username, password);
        self.list
            .insert(ProviderType::Upcloud, RobotBackend::Upcloud(api));
    }

    pub fn add_scaleway(&mut self, secret_key: &str) {
        let api = ScalewayApi::new(secret_key);
        self.list
            .insert(ProviderType::ScaleWay, RobotBackend::ScaleWay(api));
    }

    pub async fn list_os(&self) -> Result<Vec<RobotOs>, RobotError> {
        let results =
            futures::future::join_all(self.list.values().map(|backend| backend.list_os())).await;
        let list = results
            .into_iter()
            .collect::<Result<Vec<Vec<RobotOs>>, RobotError>>()?;
        Ok(list.into_iter().flatten().collect())
    }

    pub async fn list_machines(&self) -> Result<Vec<RobotMachine>, RobotError> {
        let results =
            futures::future::join_all(self.list.values().map(|backend| backend.list_machines()))
                .await;
        let list = results
            .into_iter()
            .collect::<Result<Vec<Vec<RobotMachine>>, RobotError>>()?;
        Ok(list.into_iter().flatten().collect())
    }

    pub async fn list_releases(&self) -> Result<Vec<RobotRelease>, RobotError> {
        let results =
            futures::future::join_all(self.list.values().map(|backend| backend.list_machines()))
                .await;
        let list = results
            .into_iter()
            .collect::<Result<Vec<Vec<RobotMachine>>, RobotError>>()?;
        let list: Vec<RobotMachine> = list.into_iter().flatten().collect();
        let result_map: HashMap<String, RobotRelease> =
            list.into_iter().fold(HashMap::new(), |mut map, machine| {
                map.entry(format!(
                    "{}/{}",
                    machine.release_name(),
                    machine.release_version()
                ))
                .and_modify(|release| release.add_machine(machine.clone()))
                .or_insert(RobotRelease::new(
                    machine.release_name(),
                    machine.release_version(),
                    vec![machine.clone()],
                ));
                map
            });
        Ok(result_map.into_values().collect::<Vec<RobotRelease>>())
    }

    pub async fn get_releases_by_name(
        &self,
        release_name: &str,
    ) -> Result<Vec<RobotRelease>, RobotError> {
        let releases = self
            .list_releases()
            .await?
            .into_iter()
            .filter(|release| release.name().eq(release_name))
            .collect();
        Ok(releases)
    }

    pub async fn list_plans(
        &self,
        min_ram: Option<u64>,
        max_ram: Option<u64>,
        min_cpu: Option<u32>,
        max_cpu: Option<u32>,
    ) -> Vec<RobotPlan> {
        let mut list: Vec<RobotPlan> = vec![];
        for item in self.list.values() {
            match item.list_plans().await {
                Ok(newlist) => {
                    list.extend(newlist);
                }
                Err(err) => {
                    error!("Unable to load: {}", err);
                }
            }
        }

        let mut plans: Vec<RobotPlan> = list
            .into_iter()
            .filter(|plan| {
                if let Some(max_cpu) = max_cpu {
                    plan.cpus() <= max_cpu
                } else {
                    true
                }
            })
            .filter(|plan| {
                if let Some(min_cpu) = min_cpu {
                    plan.cpus() >= min_cpu
                } else {
                    true
                }
            })
            .filter(|plan| {
                if let Some(max_ram) = max_ram {
                    plan.memory() <= max_ram
                } else {
                    true
                }
            })
            .filter(|plan| {
                if let Some(min_ram) = min_ram {
                    plan.memory() >= min_ram
                } else {
                    true
                }
            })
            .collect();
        plans.sort_unstable_by(|plan1, plan2| {
            plan1
                .monthly_pricing()
                .partial_cmp(&plan2.monthly_pricing())
                .unwrap()
        });
        plans
    }

    pub async fn create_machine<S1, S2>(
        &self,
        plan: &RobotPlan,
        release_name: S1,
        release_version: u32,
        image: S2,
        container_port: u32,
        vultr_os_id: u32,
        hetzner_os_id: u32,
        upcloud_os_id: String,
        digitalocean_os_id: String,
        linode_os_id: String,
        scaleway_marketplace_os_id: String,
        scaleway_default_project_id: Option<String>,
    ) -> Result<RobotMachine, RobotError>
    where
        S1: AsRef<str>,
        S2: AsRef<str>,
    {
        let backend = self
            .list
            .get(plan.provider())
            .ok_or(RobotError::BackendNotActive(plan.provider().clone()))?;
        backend
            .create_machine(
                plan,
                release_name,
                release_version,
                image,
                container_port,
                vultr_os_id,
                hetzner_os_id,
                upcloud_os_id,
                digitalocean_os_id,
                linode_os_id,
                scaleway_marketplace_os_id,
                scaleway_default_project_id,
            )
            .await
    }

    pub async fn delete_machine(&self, machine: RobotMachine) -> Result<(), RobotError> {
        let backend = self
            .list
            .get(machine.provider())
            .ok_or(RobotError::BackendNotActive(machine.provider().clone()))?;
        backend.delete_machine(machine).await?;
        Ok(())
    }

    pub async fn delete_release<S>(
        &self,
        release_name: S,
        release_version: u32,
    ) -> Result<(), RobotError>
    where
        S: AsRef<str>,
    {
        for backend in self.list.values() {
            backend
                .delete_release(&release_name, release_version)
                .await?;
        }
        Ok(())
    }

    /*
    pub async fn delete_releases<S>(&self, release_name: S) -> Result<(), RobotError>
    where
        S: AsRef<str>,
    {
        for backend in self.list.values() {
            backend.delete_releases(&release_name).await?;
        }
        Ok(())
    }
    */

    pub async fn list_dns(&self) -> Result<(), RobotError> {
        for backend in self.list.values() {
            backend.list_dns().await?;
        }
        Ok(())
    }

    pub async fn get_machine(&self, machine: &RobotMachine) -> Result<RobotMachine, RobotError> {
        let provider = self.list.get(machine.provider());
        if let Some(backend) = provider {
            Ok(backend.get_machine(machine).await?)
        } else {
            Err(RobotError::UnknownProvider)
        }
    }

    pub async fn get_dns_domain<S: AsRef<str>>(
        &self,
        domainname: S,
    ) -> Result<Vec<RobotDomain>, RobotError> {
        let results = futures::future::join_all(
            self.list
                .values()
                .map(|backend| backend.get_dns_domain(domainname.as_ref())),
        )
        .await;
        let list = results
            .into_iter()
            .collect::<Result<Vec<Vec<RobotDomain>>, RobotError>>()?;
        Ok(list.into_iter().flatten().collect())
    }

    pub async fn delete_dns_name(&self, hostname: &str) -> Result<(), RobotError> {
        for backend in self.list.values() {
            backend.delete_dns_name(hostname).await?;
        }
        Ok(())
    }
}
