use super::robotlib::ProviderType;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum RobotMachineState {
    Stopped,
    Started,
    Error,
    Transitioning,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct RobotMachine {
    provider: ProviderType,
    id: String,
    name: String,
    release_name: String,
    release_version: u32,
    pub ipv4: Option<String>,
    pub ipv6: Option<String>,
    state: RobotMachineState,
    location: String,
}

impl RobotMachine {
    pub fn new(
        provider: ProviderType,
        id: String,
        name: String,
        release_name: String,
        release_version: u32,
        ipv4: Option<String>,
        ipv6: Option<String>,
        state: RobotMachineState,
        location: String,
    ) -> Self {
        RobotMachine {
            provider,
            id,
            name,
            release_name,
            release_version,
            ipv4,
            ipv6,
            state,
            location,
        }
    }

    pub fn provider(&self) -> &ProviderType {
        &self.provider
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn release_name(&self) -> &String {
        &self.release_name
    }

    pub fn release_version(&self) -> u32 {
        self.release_version
    }

    pub fn state(&self) -> &RobotMachineState {
        &self.state
    }

    pub fn location(&self) -> &String {
        &self.location
    }
}
