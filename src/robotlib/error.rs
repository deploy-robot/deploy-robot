use digitalocean_rs::DigitalOceanError;
use hcloud::apis::{
    images_api::ListImagesError,
    server_types_api::ListServerTypesError,
    servers_api::{
        CreateServerError, DeleteServerError, GetServerError, ListServersError, ShutdownServerError,
    },
};
use linode_rs::LinodeError;
use scaleway_rs::ScalewayError;
use std::error::Error;
use upcloud_rs::UpcloudError;
use vultr::VultrError;

use super::robotlib::ProviderType;

#[derive(Debug)]
pub enum RobotError {
    DigitalOcean(DigitalOceanError),
    Linode(LinodeError),
    Vultr(VultrError),
    Hetzner(String),
    ScaleWay(ScalewayError),
    BackendNotActive(ProviderType),
    Upcloud(UpcloudError),
    NotManagedByDeployRobot,
    DNSNotSupportedByProvider,
    MachineNotFound,
    RegionNotFound,
    UnknownProvider,
}

impl std::fmt::Display for RobotError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "RobotError")
    }
}

impl Error for RobotError {}

impl From<VultrError> for RobotError {
    fn from(value: VultrError) -> Self {
        RobotError::Vultr(value)
    }
}

impl From<hcloud::apis::Error<ListServerTypesError>> for RobotError {
    fn from(value: hcloud::apis::Error<ListServerTypesError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<hcloud::apis::Error<ListImagesError>> for RobotError {
    fn from(value: hcloud::apis::Error<ListImagesError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<hcloud::apis::Error<ListServersError>> for RobotError {
    fn from(value: hcloud::apis::Error<ListServersError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<hcloud::apis::Error<GetServerError>> for RobotError {
    fn from(value: hcloud::apis::Error<GetServerError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<hcloud::apis::Error<ShutdownServerError>> for RobotError {
    fn from(value: hcloud::apis::Error<ShutdownServerError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<hcloud::apis::Error<DeleteServerError>> for RobotError {
    fn from(value: hcloud::apis::Error<DeleteServerError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<hcloud::apis::Error<CreateServerError>> for RobotError {
    fn from(value: hcloud::apis::Error<CreateServerError>) -> Self {
        RobotError::Hetzner(value.to_string())
    }
}

impl From<UpcloudError> for RobotError {
    fn from(value: UpcloudError) -> Self {
        RobotError::Upcloud(value)
    }
}

impl From<ScalewayError> for RobotError {
    fn from(value: ScalewayError) -> Self {
        RobotError::ScaleWay(value)
    }
}

impl From<DigitalOceanError> for RobotError {
    fn from(value: DigitalOceanError) -> Self {
        RobotError::DigitalOcean(value)
    }
}

impl From<LinodeError> for RobotError {
    fn from(value: LinodeError) -> Self {
        RobotError::Linode(value)
    }
}
