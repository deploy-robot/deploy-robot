mod backend;
mod dns_domain;
mod dns_record;
mod error;
mod machine;
mod os;
mod plan;
mod providers;
mod release;
mod robotlib;

pub use dns_domain::RobotDomain;
pub use dns_record::RobotDomainEntry;
pub use error::RobotError;
pub use machine::RobotMachine;
pub use machine::RobotMachineState;
pub use os::RobotOs;
pub use plan::RobotPlan;
pub use robotlib::RobotLib;

static DEPLOY_ROBOT_TAG_MARKER_NAME: &str = "DeployRobot-Name";
static DEPLOY_ROBOT_TAG_MARKER_VERSION: &str = "DeployRobot-Version";
