use super::{backend::RobotBackend, RobotError};

pub struct RobotDomainEntry {
    pub api: RobotBackend,
    pub domainname: String,
    pub entryid: Option<String>,
    pub entryname: String,
    pub data: String,
    pub ttl: u64,
}

impl RobotDomainEntry {
    pub async fn update_data<S: Into<String>>(&mut self, data: S) -> Result<(), RobotError> {
        self.data = data.into();
        if let Some(entryid) = &self.entryid {
            match &self.api {
                RobotBackend::Vultr(api) => {
                    api.update_dns_record(&self.domainname, entryid)
                        .data(&self.data)
                        .run_async()
                        .await?;
                }
                _ => {}
            };
        }
        Ok(())
    }
}
