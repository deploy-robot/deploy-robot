use super::robotlib::ProviderType;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct RobotOs {
    provider: ProviderType,
    id: String,
    family: Option<String>,
    name: String,
    location: Option<String>,
}



impl RobotOs {
    pub fn new(
        provider: ProviderType,
        id: String,
        family: Option<String>,
        name: String,
        location: Option<String>,
    ) -> Self {
        RobotOs {
            provider,
            id,
            family,
            name,
            location,
        }
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    pub fn family(&self) -> &Option<String> {
        &self.family
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn provider(&self) -> &ProviderType {
        &self.provider
    }
}
