use byte_unit::{Byte, UnitType};

use super::robotlib::ProviderType;

#[derive(Clone, Debug, PartialEq)]
pub struct RobotPlan {
    provider: ProviderType,
    id: String,
    cpus: u32,
    memory: u64,
    included_disk: Option<u64>,
    included_traffic: Option<u64>,
    monthly_pricing: f64,
    location: String,
}

impl RobotPlan {
    pub fn new(
        provider: ProviderType,
        id: String,
        cpus: u32,
        memory: u64,
        included_disk: Option<u64>,
        included_traffic: Option<u64>,
        monthly_pricing: f64,
        location: String,
    ) -> Self {
        RobotPlan {
            provider,
            id,
            cpus,
            memory,
            included_disk,
            included_traffic,
            monthly_pricing,
            location,
        }
    }

    pub fn provider(&self) -> &ProviderType {
        &self.provider
    }
    pub fn id(&self) -> &str {
        &self.id
    }
    pub fn cpus(&self) -> u32 {
        self.cpus
    }
    pub fn memory(&self) -> u64 {
        self.memory
    }
    pub fn memory_human(&self) -> String {
        let b = Byte::from_u64(self.memory);
        b.get_appropriate_unit(UnitType::Decimal).to_string()
    }
    pub fn included_disk(&self) -> Option<u64> {
        self.included_disk
    }
    pub fn disk_human(&self) -> Option<String> {
        self.included_disk.map(|included_disk| {
            let b = Byte::from_u64(included_disk);
            b.get_appropriate_unit(UnitType::Decimal).to_string()
        })
    }
    pub fn included_traffic(&self) -> Option<u64> {
        self.included_traffic
    }
    pub fn traffic_human(&self) -> Option<String> {
        self.included_traffic.map(|included_traffic| {
            let b = Byte::from_u64(included_traffic);
            b.get_appropriate_unit(UnitType::Decimal).to_string()
        })
    }
    pub fn monthly_pricing(&self) -> f64 {
        self.monthly_pricing
    }
    pub fn location(&self) -> &str {
        &self.location
    }
}
