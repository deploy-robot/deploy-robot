use digitalocean_rs::{
    DigitalOceanApi, DigitalOceanDroplet, DigitalOceanImage, DigitalOceanSize, ImageType,
};

use crate::robotlib::{
    machine::{RobotMachine, RobotMachineState},
    robotlib::ProviderType,
    RobotError, RobotOs, RobotPlan, DEPLOY_ROBOT_TAG_MARKER_NAME, DEPLOY_ROBOT_TAG_MARKER_VERSION,
};

use super::vec2map;

pub async fn list_os(api: &DigitalOceanApi) -> Result<Vec<RobotOs>, RobotError> {
    Ok(api
        .list_images()
        .image_type(ImageType::Distribution)
        .page(1)
        .per_page(50)
        .run_async()
        .await?
        .into_iter()
        .map(|os| os.into())
        .collect())
}

pub async fn list_machines(api: &DigitalOceanApi) -> Result<Vec<RobotMachine>, RobotError> {
    Ok(api
        .list_droplets_async()
        .await?
        .into_iter()
        .filter_map(|item| item.try_into().ok())
        .collect())
}

pub async fn get_machine(
    api: &DigitalOceanApi,
    machine_id: &str,
) -> Result<RobotMachine, RobotError> {
    Ok(api.get_droplet_async(machine_id).await?.try_into()?)
}

pub async fn list_plans(api: &DigitalOceanApi) -> Result<Vec<RobotPlan>, RobotError> {
    let plans: Vec<RobotPlan> = api
        .list_sizes_async()
        .await?
        .into_iter()
        .map(|plan| convert_plan(plan))
        .flatten()
        .collect();
    Ok(plans)
}

fn convert_plan(plan: DigitalOceanSize) -> Vec<RobotPlan> {
    let list: Vec<RobotPlan> = plan
        .regions
        .iter()
        .filter_map(|location| {
            if plan.available {
                Some(RobotPlan::new(
                    ProviderType::DigitalOcean,
                    plan.slug.to_string(),
                    plan.vcpus as u32,
                    (plan.memory as u64) * 1000 * 1000, // MB
                    Some((plan.disk as u64) * 1000 * 1000 * 1000), // GB
                    Some(((plan.transfer as f32) * 1000f32 * 1000f32 * 1000f32 * 1000f32) as u64), // TB
                    plan.price_monthly as f64,
                    location.to_string(),
                ))
            } else {
                None
            }
        })
        .collect();
    list
}

impl From<DigitalOceanImage> for RobotOs {
    fn from(value: DigitalOceanImage) -> Self {
        RobotOs::new(
            ProviderType::DigitalOcean,
            value.id.to_string(),
            Some(value.distribution),
            value.description,
            None,
        )
    }
}

impl TryInto<RobotMachine> for DigitalOceanDroplet {
    type Error = RobotError;

    fn try_into(self) -> Result<RobotMachine, Self::Error> {
        if let Some((release_name, release_version)) = vec2map(self.tags, ":") {
            let state: RobotMachineState = match self.status.as_str() {
                "active" => RobotMachineState::Started,
                _ => RobotMachineState::Stopped,
            };
            return Ok(RobotMachine::new(
                ProviderType::DigitalOcean,
                self.id.to_string(),
                self.name,
                release_name.to_string(),
                release_version,
                self.networks
                    .v4
                    .first()
                    .map(|item| item.ip_address.to_string()),
                self.networks
                    .v6
                    .first()
                    .map(|item| item.ip_address.to_string()),
                state,
                self.region.slug,
            ));
        }
        Err(RobotError::NotManagedByDeployRobot)
    }
}

pub async fn create_machine<S1: AsRef<str>, S2: AsRef<str>, S3: AsRef<str>>(
    api: &DigitalOceanApi,
    release_name: S1,
    release_version: u32,
    user_data: S2,
    os_id: S3,
    plan: &RobotPlan,
) -> Result<RobotMachine, RobotError> {
    let machine = api
        .create_droplet(release_name.as_ref(), plan.id(), os_id.as_ref())
        .user_data(user_data.as_ref())
        .region(plan.location())
        .ipv6(true)
        .tags(vec![
            format!("{}:{}", DEPLOY_ROBOT_TAG_MARKER_NAME, release_name.as_ref()),
            format!("{}:{}", DEPLOY_ROBOT_TAG_MARKER_VERSION, release_version),
        ])
        .run_async()
        .await?;
    Ok(machine.try_into()?)
}
