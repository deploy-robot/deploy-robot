use std::collections::HashMap;

use crate::robotlib::{
    machine::{RobotMachine, RobotMachineState},
    robotlib::ProviderType,
    RobotError, RobotOs, RobotPlan, DEPLOY_ROBOT_TAG_MARKER_NAME, DEPLOY_ROBOT_TAG_MARKER_VERSION,
};
use upcloud_rs::{
    UpcloudApi, UpcloudPlan, UpcloudPricesZone, UpcloudServer, UpcloudServerTemplate,
};

pub async fn list_os(api: &UpcloudApi) -> Result<Vec<RobotOs>, RobotError> {
    let images: Vec<RobotOs> = api
        .get_server_templates_async()
        .await?
        .into_iter()
        .map(|item| item.into())
        .collect();
    Ok(images)
}

pub async fn list_machines(api: &UpcloudApi) -> Result<Vec<RobotMachine>, RobotError> {
    Ok(api
        .get_servers_async()
        .await?
        .into_iter()
        .filter_map(|item| item.try_into().ok())
        .collect())
}

pub async fn get_machine(api: &UpcloudApi, machine_id: &str) -> Result<RobotMachine, RobotError> {
    Ok(api.get_server_details_async(machine_id).await?.try_into()?)
}

pub async fn list_plans(api: &UpcloudApi) -> Result<Vec<RobotPlan>, RobotError> {
    let pricing = api.get_prices_async().await?;
    let plans = api
        .get_plans_async()
        .await?
        .into_iter()
        .map(|plan| convert_plan(plan, &pricing))
        .flatten()
        .collect();
    Ok(plans)
}

impl From<UpcloudServerTemplate> for RobotOs {
    fn from(value: UpcloudServerTemplate) -> Self {
        RobotOs::new(ProviderType::Upcloud, value.uuid, None, value.title, None)
    }
}

fn convert_plan(plan: UpcloudPlan, pricing: &Vec<UpcloudPricesZone>) -> Vec<RobotPlan> {
    let list: Vec<RobotPlan> = pricing
        .iter()
        .map(|price| {
            RobotPlan::new(
                ProviderType::Upcloud,
                plan.name.to_string(),
                plan.core_number as u32,
                plan.memory_amount as u64 * (1000 * 1000), // MB -> Byte
                Some(plan.storage_size as u64 * (1000 * 1000 * 1000)), // GB -> Byte
                Some(plan.public_traffic_out as u64 * (1000 * 1000 * 1000)), // GB -> Byte
                (price
                    .extra
                    .get(&format!("server_plan_{}", plan.name))
                    .unwrap()
                    .price as f64)
                    * (24f64 * 28f64 / 100f64),
                price.name.to_string(),
            )
        })
        .collect();
    list
}

impl TryInto<RobotMachine> for UpcloudServer {
    type Error = RobotError;

    fn try_into(self) -> Result<RobotMachine, Self::Error> {
        let hashmap: HashMap<String, String> = self
            .labels
            .label
            .into_iter()
            .map(|item| (item.key, item.value))
            .collect();
        let release_name = hashmap.get(DEPLOY_ROBOT_TAG_MARKER_NAME);
        let release_version = hashmap
            .get(DEPLOY_ROBOT_TAG_MARKER_VERSION)
            .map(|item| item.parse::<u32>().ok())
            .flatten();
        if let Some(release_name) = release_name {
            if let Some(release_version) = release_version {
                let state: RobotMachineState = match self.state.as_str() {
                    "started" => RobotMachineState::Started,
                    "stopped" => RobotMachineState::Stopped,
                    "maintenance" => RobotMachineState::Transitioning,
                    "error" => RobotMachineState::Error,
                    _ => RobotMachineState::Stopped,
                };
                return Ok(RobotMachine::new(
                    ProviderType::Upcloud,
                    self.uuid,
                    self.hostname,
                    release_name.to_string(),
                    release_version,
                    None,
                    None,
                    state,
                    self.zone,
                ));
            }
        }
        Err(RobotError::NotManagedByDeployRobot)
    }
}

pub async fn create_machine<S1: AsRef<str>, S2: AsRef<str>, S3: AsRef<str>>(
    api: &UpcloudApi,
    release_name: S1,
    release_version: u32,
    user_data: S2,
    os_id: S3,
    plan: &RobotPlan,
) -> Result<RobotMachine, RobotError> {
    let mut labels = HashMap::new();
    labels.insert(
        String::from(DEPLOY_ROBOT_TAG_MARKER_NAME),
        String::from(release_name.as_ref()),
    );
    labels.insert(
        String::from(DEPLOY_ROBOT_TAG_MARKER_VERSION),
        release_version.to_string(),
    );
    let instance = api
        .create_instance(plan.location(), plan.id(), os_id.as_ref(), "abc", "def")
        .user_data(user_data.as_ref())
        .labels(labels)
        .run_async()
        .await?;
    Ok(instance.try_into()?)
}
