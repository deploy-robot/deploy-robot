use std::collections::HashMap;

use super::{DEPLOY_ROBOT_TAG_MARKER_NAME, DEPLOY_ROBOT_TAG_MARKER_VERSION};

pub mod digitalocean;
pub mod hetzner;
pub mod linode;
pub mod scaleway;
pub mod upcloud;
pub mod vultr;

pub fn vec2map(tags: Vec<String>, split_char: &str) -> Option<(String, u32)> {
    let hashmap: HashMap<_, _> = tags
        .into_iter()
        .map(|s| {
            s.split(split_char)
                .map(|s| s.to_string())
                .collect::<Vec<String>>()
        })
        .filter(|items| items.len() == 2)
        .map(|items| (items[0].to_string(), items[1].to_string()))
        .collect();

    let release_name = hashmap.get(DEPLOY_ROBOT_TAG_MARKER_NAME);
    let release_version = hashmap
        .get(DEPLOY_ROBOT_TAG_MARKER_VERSION)
        .map(|s| s.parse::<u32>().ok())
        .flatten();

    if let Some(release_name) = release_name {
        if let Some(release_version) = release_version {
            return Some((release_name.to_string(), release_version));
        }
    }
    None
}
