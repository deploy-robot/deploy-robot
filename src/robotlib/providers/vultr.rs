use crate::robotlib::{
    backend::RobotBackend,
    machine::{RobotMachine, RobotMachineState},
    robotlib::ProviderType,
    RobotDomain, RobotDomainEntry, RobotError, RobotOs, RobotPlan, DEPLOY_ROBOT_TAG_MARKER_NAME,
    DEPLOY_ROBOT_TAG_MARKER_VERSION,
};
use vultr::{
    VultrApi, VultrDomain, VultrDomainRecord, VultrInstance, VultrInstanceType, VultrOS, VultrPlan,
};

use super::vec2map;

pub async fn list_os(api: &VultrApi) -> Result<Vec<RobotOs>, RobotError> {
    Ok(api
        .get_os_list_async()
        .await?
        .into_iter()
        .map(|os| os.into())
        .collect())
}

pub async fn list_machines(api: &VultrApi) -> Result<Vec<RobotMachine>, RobotError> {
    Ok(api
        .get_instance_list_async()
        .await?
        .into_iter()
        .filter_map(|item| item.try_into().ok())
        .collect())
}

pub async fn get_machine(api: &VultrApi, machine_id: &str) -> Result<RobotMachine, RobotError> {
    Ok(api.get_instance_async(machine_id).await?.try_into()?)
}

pub async fn list_plans(api: &VultrApi) -> Result<Vec<RobotPlan>, RobotError> {
    let plans: Vec<RobotPlan> = api
        .get_plans_async()
        .await?
        .into_iter()
        .map(|plan| convert_plan(plan))
        .flatten()
        .collect();
    Ok(plans)
}

fn convert_plan(plan: VultrPlan) -> Vec<RobotPlan> {
    let list: Vec<RobotPlan> = plan
        .locations
        .iter()
        .map(|location| {
            RobotPlan::new(
                ProviderType::Vultr,
                plan.id.to_string(),
                plan.vcpu_count as u32,
                plan.ram as u64 * 1000 * 1000, // MB -> Byte
                Some((plan.disk * 1000f32).round() as u64 * (1000 * 1000)), // GB -> Byte
                Some((plan.bandwidth * 1000f32).round() as u64 * (1000 * 1000)), // GB -> Byte
                plan.monthly_cost as f64,
                location.to_string(),
            )
        })
        .collect();
    list
}

impl From<VultrOS> for RobotOs {
    fn from(value: VultrOS) -> Self {
        RobotOs::new(
            ProviderType::Vultr,
            value.id.to_string(),
            Some(value.family),
            value.name,
            None,
        )
    }
}

impl TryInto<RobotMachine> for VultrInstance {
    type Error = RobotError;

    fn try_into(self) -> Result<RobotMachine, Self::Error> {
        if let Some((release_name, release_version)) = vec2map(self.tags, "=") {
            let state: RobotMachineState = match self.power_status.as_str() {
                "running" => RobotMachineState::Started,
                _ => RobotMachineState::Stopped,
            };
            return Ok(RobotMachine::new(
                ProviderType::Vultr,
                self.id,
                self.label,
                release_name.to_string(),
                release_version,
                Some(self.main_ip),
                Some(self.v6_main_ip),
                state,
                self.region.to_string(),
            ));
        }
        Err(RobotError::NotManagedByDeployRobot)
    }
}

pub async fn create_machine<S1: AsRef<str>, S2: AsRef<str>>(
    api: &VultrApi,
    release_name: S1,
    release_version: u32,
    user_data: S2,
    os_id: u32,
    plan: &RobotPlan,
) -> Result<RobotMachine, RobotError> {
    let machine = api
        .create_instance(plan.location(), plan.id(), VultrInstanceType::OS(os_id))
        .enable_ipv6(true)
        .tags(vec![
            format!("{}={}", DEPLOY_ROBOT_TAG_MARKER_NAME, release_name.as_ref()),
            format!("{}={}", DEPLOY_ROBOT_TAG_MARKER_VERSION, release_version),
        ])
        .user_data(user_data.as_ref())
        .run_async()
        .await?;
    Ok(machine.try_into()?)
}

pub fn domain2robot(api: RobotBackend, value: VultrDomain) -> RobotDomain {
    RobotDomain {
        api,
        provider: ProviderType::Vultr,
        name: value.domain,
    }
}

pub async fn get_dns_domain<S: AsRef<str>>(
    api: &VultrApi,
    domainname: S,
) -> Result<VultrDomain, RobotError> {
    Ok(api.get_dns_domain_async(domainname.as_ref()).await?)
}

pub fn domainentry2robot(
    api: RobotBackend,
    domainname: &str,
    value: VultrDomainRecord,
) -> RobotDomainEntry {
    RobotDomainEntry {
        domainname: domainname.to_string(),
        entryid: Some(value.id),
        entryname: value.name,
        data: value.data,
        ttl: value.ttl as u64,
        api: api,
    }
}

pub async fn delete_dns_name(
    api: &VultrApi,
    domainname: &str,
    entryname: &str,
) -> Result<(), RobotError> {
    let records = api.get_dns_domain_records_async(domainname).await?;
    for record in records {
        if record.name.eq(entryname) {
            api.delete_dns_domain_record_async(domainname, record.id)
                .await?;
        }
    }
    Ok(())
}
