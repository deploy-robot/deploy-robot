use scaleway_rs::{ScalewayApi, ScalewayInstance, ScalewayMarketplaceImage, ServerType};

use crate::robotlib::{
    machine::{RobotMachine, RobotMachineState},
    robotlib::ProviderType,
    RobotError, RobotOs, RobotPlan, DEPLOY_ROBOT_TAG_MARKER_NAME, DEPLOY_ROBOT_TAG_MARKER_VERSION,
};

use super::vec2map;

pub async fn list_os(api: &ScalewayApi) -> Result<Vec<RobotOs>, RobotError> {
    let mut images = vec![];
    let list: Vec<_> = api
        .list_marketplace_instances()
        .category("distribution")
        .arch("x86_64")
        .include_eol(false)
        .run_async()
        .await?
        .into_iter()
        .map(|item| item.into())
        .collect();
    images.extend(list.into_iter());
    Ok(images)
}

pub async fn list_machines(api: &ScalewayApi) -> Result<Vec<RobotMachine>, RobotError> {
    let mut instances = vec![];
    for zone in ScalewayApi::az_list() {
        let l: Vec<_> = api
            .list_instances(zone)
            .run_async()
            .await?
            .into_iter()
            .filter_map(|item| item.try_into().ok())
            .collect();
        instances.extend(l.into_iter());
    }
    Ok(instances)
}

pub async fn get_machine(
    api: &ScalewayApi,
    zone_id: &str,
    machine_id: &str,
) -> Result<RobotMachine, RobotError> {
    Ok(api
        .get_instance_async(zone_id, machine_id)
        .await?
        .try_into()?)
}

pub async fn list_plans(api: &ScalewayApi) -> Result<Vec<RobotPlan>, RobotError> {
    let mut plans = vec![];
    for zone in ScalewayApi::az_list() {
        let list: Vec<RobotPlan> = api
            .get_server_types_async(zone)
            .await?
            .into_iter()
            .map(|plan| convert_plan(plan))
            .flatten()
            .collect();
        plans.extend(list.into_iter());
    }
    Ok(plans)
}

pub async fn delete_instance(api: &ScalewayApi, machine: &RobotMachine) -> Result<(), RobotError> {
    let instance = api
        .get_instance_async(&machine.location(), machine.id())
        .await?;
    api.delete_instance_async(machine.location(), machine.id())
        .await?;
    for volume in instance.volumes.volumes.values() {
        api.delete_volume_async(machine.location(), &volume.id)
            .await?;
    }
    Ok(())
}

impl From<ScalewayMarketplaceImage> for RobotOs {
    fn from(value: ScalewayMarketplaceImage) -> Self {
        RobotOs::new(
            ProviderType::ScaleWay,
            value.id,
            Some(value.label),
            value.name,
            None,
        )
    }
}

fn convert_plan(plan: ServerType) -> Vec<RobotPlan> {
    vec![RobotPlan::new(
        ProviderType::ScaleWay,
        plan.id,
        plan.ncpus as u32,
        plan.ram as u64,
        None,
        None,
        plan.monthly_price as f64,
        plan.location,
    )]
}

impl TryInto<RobotMachine> for ScalewayInstance {
    type Error = RobotError;

    fn try_into(self) -> Result<RobotMachine, Self::Error> {
        if let Some((release_name, release_version)) = vec2map(self.tags, "=") {
            let state: RobotMachineState = match self.state.as_str() {
                "running" => RobotMachineState::Started,
                "stopped" => RobotMachineState::Stopped,
                "stopped in place" => RobotMachineState::Stopped,
                "starting" => RobotMachineState::Transitioning,
                "stopping" => RobotMachineState::Transitioning,
                "locked" => RobotMachineState::Error,
                _ => RobotMachineState::Error,
            };

            return Ok(RobotMachine::new(
                ProviderType::ScaleWay,
                self.id.to_string(),
                self.name,
                release_name.to_string(),
                release_version,
                None,
                None,
                state,
                self.zone,
            ));
        }
        Err(RobotError::NotManagedByDeployRobot)
    }
}

pub async fn create_machine<S1: AsRef<str>, S2: AsRef<str>, S3: AsRef<str>, S4: AsRef<str>>(
    api: &ScalewayApi,
    release_name: S1,
    release_version: u32,
    user_data: S2,
    os_id: S3,
    default_project_id: Option<S4>,
    plan: &RobotPlan,
) -> Result<RobotMachine, RobotError> {
    let default_project_id =
        default_project_id.expect("Default project ID is needed for scaleway!");
    let images = api
        .list_marketplace_instances()
        .category("distribution")
        .arch("x86_64")
        .include_eol(false)
        .run_async()
        .await?;
    let image = images
        .into_iter()
        .filter(|image| image.id.eq(os_id.as_ref()))
        .last();
    if let Some(image) = image {
        println!("I {:#?}", image);
        let local_images = api
            .list_marketplace_local_images(scaleway_rs::LocalImageListType::ByImageId(
                image.id.to_string(),
            ))
            .run_async()
            .await?;
        let local_image = local_images
            .into_iter()
            .filter(|local| local.zone.eq(plan.location()) && local.arch.eq("x86_64"))
            .last();
        println!("L {:#?}", local_image);
        if let Some(last) = local_image {
            println!("found last: {:#?}", last);
            let instance = api
                .create_instance(plan.location(), release_name.as_ref(), plan.id())
                .enable_ipv6(true)
                .image(&last.id)
                .project(default_project_id.as_ref())
                .tags(vec![
                    format!("{}={}", DEPLOY_ROBOT_TAG_MARKER_NAME, release_name.as_ref()),
                    format!("{}={}", DEPLOY_ROBOT_TAG_MARKER_VERSION, release_version),
                ])
                .run_async()
                .await?;
            println!("instance created: {:#?}", instance);
            api.set_userdata_async(
                plan.location(),
                &instance.id,
                "cloud-init",
                user_data.as_ref(),
            )
            .await?;
            println!("userdata set");
            api.perform_instance_action_async(plan.location(), &instance.id, "poweron")
                .await?;
            println!("action started");
            return Ok(instance.try_into()?);
        } else {
            println!("os version image not found");
            return Err(RobotError::UnknownProvider);
        }
    } else {
        println!("ubuntu_jammy image not found");
        return Err(RobotError::UnknownProvider);
    }
}
