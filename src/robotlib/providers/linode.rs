use linode_rs::{LinodeApi, LinodeInstance, LinodeOs, LinodeRegion, LinodeType};
use passwords::PasswordGenerator;

use crate::robotlib::{
    machine::{RobotMachine, RobotMachineState},
    robotlib::ProviderType,
    RobotError, RobotOs, RobotPlan, DEPLOY_ROBOT_TAG_MARKER_NAME, DEPLOY_ROBOT_TAG_MARKER_VERSION,
};

use super::vec2map;

pub async fn list_os(api: &LinodeApi) -> Result<Vec<RobotOs>, RobotError> {
    Ok(api
        .list_os_async()
        .await?
        .into_iter()
        .map(|os| os.into())
        .collect())
}

pub async fn list_machines(api: &LinodeApi) -> Result<Vec<RobotMachine>, RobotError> {
    Ok(api
        .list_instances_async()
        .await?
        .into_iter()
        .filter_map(|item| item.try_into().ok())
        .collect())
}

pub async fn get_machine(api: &LinodeApi, machine_id: u64) -> Result<RobotMachine, RobotError> {
    Ok(api.get_instance_async(machine_id).await?.try_into()?)
}

pub async fn list_plans(api: &LinodeApi) -> Result<Vec<RobotPlan>, RobotError> {
    let regions: Vec<LinodeRegion> = api.list_regions_async().await?;
    let plans: Vec<RobotPlan> = api
        .list_types_async()
        .await?
        .into_iter()
        .map(|plan| convert_plan(plan, &regions))
        .flatten()
        .collect();
    Ok(plans)
}

fn convert_plan(plan: LinodeType, regions: &Vec<LinodeRegion>) -> Vec<RobotPlan> {
    let list: Vec<RobotPlan> = regions
        .iter()
        .filter_map(|region| {
            let mut monthly_pricing = plan.price.monthly;
            for region_price in plan.region_prices.iter() {
                if region_price.id.eq(&region.id) {
                    monthly_pricing = region_price.monthly;
                }
            }
            Some(RobotPlan::new(
                ProviderType::Linode,
                plan.id.to_string(),
                plan.vcpus as u32,
                plan.memory as u64 * 1000 * 1000,     // MB -> Byte
                Some(plan.disk as u64 * 1000 * 1000), // MB -> Byte
                Some(plan.transfer * 1000 * 1000 * 1000), // GB -> Byte
                monthly_pricing as f64,
                region.id.to_string(),
            ))
        })
        .collect();
    list
}

impl From<LinodeOs> for RobotOs {
    fn from(value: LinodeOs) -> Self {
        RobotOs::new(
            ProviderType::Linode,
            value.id.to_string(),
            value.description,
            value.label,
            None,
        )
    }
}

impl TryInto<RobotMachine> for LinodeInstance {
    type Error = RobotError;

    fn try_into(self) -> Result<RobotMachine, Self::Error> {
        if let Some((release_name, release_version)) = vec2map(self.tags, ":") {
            let state: RobotMachineState = match self.status.as_str() {
                "running" => RobotMachineState::Started,
                _ => RobotMachineState::Stopped,
            };
            return Ok(RobotMachine::new(
                ProviderType::Linode,
                self.id.to_string(),
                self.label,
                release_name.to_string(),
                release_version,
                self.ipv4.first().map(|item| item.to_string()),
                Some(self.ipv6),
                state,
                self.region,
            ));
        }
        Err(RobotError::NotManagedByDeployRobot)
    }
}

pub async fn create_machine<S1: AsRef<str>, S2: AsRef<str>, S3: AsRef<str>>(
    api: &LinodeApi,
    release_name: S1,
    release_version: u32,
    user_data: S2,
    os_id: S3,
    plan: &RobotPlan,
) -> Result<RobotMachine, RobotError> {
    let pg = PasswordGenerator::new()
        .length(20)
        .numbers(true)
        .lowercase_letters(true)
        .uppercase_letters(true)
        .symbols(true)
        .spaces(true)
        .exclude_similar_characters(true)
        .strict(true);
    let password = pg.generate_one().unwrap();
    let instance = api
        .create_instance(plan.location(), plan.id())
        .root_pass(&password)
        .image(os_id.as_ref())
        .tags(vec![
            format!("{}:{}", DEPLOY_ROBOT_TAG_MARKER_NAME, release_name.as_ref()),
            format!("{}:{}", DEPLOY_ROBOT_TAG_MARKER_VERSION, release_version),
        ])
        .user_data(user_data.as_ref())
        .run_async()
        .await?;
    Ok(instance.try_into()?)
}
