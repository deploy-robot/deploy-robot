use std::collections::HashMap;

use crate::robotlib::{
    machine::{RobotMachine, RobotMachineState},
    robotlib::ProviderType,
    RobotError, RobotOs, RobotPlan, DEPLOY_ROBOT_TAG_MARKER_NAME, DEPLOY_ROBOT_TAG_MARKER_VERSION,
};
use hcloud::{
    apis::{
        configuration::Configuration,
        images_api, server_types_api,
        servers_api::{self, CreateServerParams, GetServerParams},
    },
    models::{server::Status, CreateServerRequest, Image, Server, ServerType},
};

pub async fn list_os(config: &Configuration) -> Result<Vec<RobotOs>, RobotError> {
    let images: Vec<RobotOs> = images_api::list_images(config, Default::default())
        .await?
        .images
        .into_iter()
        .map(|item| item.into())
        .collect();
    Ok(images)
}

pub async fn list_machines(config: &Configuration) -> Result<Vec<RobotMachine>, RobotError> {
    Ok(servers_api::list_servers(config, Default::default())
        .await?
        .servers
        .into_iter()
        .filter_map(|item| item.try_into().ok())
        .collect())
}

pub async fn get_machine(
    config: &Configuration,
    machine_id: &str,
) -> Result<RobotMachine, RobotError> {
    let params = GetServerParams {
        id: machine_id.parse().unwrap(),
    };
    Ok((*servers_api::get_server(config, params)
        .await?
        .server
        .ok_or(RobotError::MachineNotFound)?)
    .try_into()?)
}

pub async fn list_plans(config: &Configuration) -> Result<Vec<RobotPlan>, RobotError> {
    let plans: Vec<RobotPlan> = server_types_api::list_server_types(&config, Default::default())
        .await?
        .server_types
        .into_iter()
        .map(|plan| convert_plan(plan))
        .flatten()
        .collect();
    Ok(plans)
}

fn convert_plan(plan: ServerType) -> Vec<RobotPlan> {
    let list: Vec<RobotPlan> = plan
        .prices
        .iter()
        .map(|price| {
            RobotPlan::new(
                ProviderType::Hetzner,
                plan.id.to_string(),
                plan.cores as u32,
                plan.memory as u64 * (1000 * 1000 * 1000), // GB -> Byte
                Some(plan.disk as u64 * (1000 * 1000 * 1000)),   // GB -> Byte
                Some(plan.included_traffic as u64),
                price.price_monthly.gross.parse::<f64>().unwrap(),
                price.location.to_string(),
            )
        })
        .collect();
    list
}

impl From<Image> for RobotOs {
    fn from(value: Image) -> Self {
        let family = match value.os_flavor {
            hcloud::models::image::OsFlavor::Alma => "alma",
            hcloud::models::image::OsFlavor::Centos => "centos",
            hcloud::models::image::OsFlavor::Debian => "debian",
            hcloud::models::image::OsFlavor::Fedora => "fedora",
            hcloud::models::image::OsFlavor::Rocky => "rocky",
            hcloud::models::image::OsFlavor::Ubuntu => "ubuntu",
            hcloud::models::image::OsFlavor::Unknown => "unknown",
        };
        RobotOs::new(
            ProviderType::Hetzner,
            value.id.to_string(),
            Some(family.to_string()),
            value.description,
            None,
        )
    }
}

impl TryInto<RobotMachine> for Server {
    type Error = RobotError;

    fn try_into(self) -> Result<RobotMachine, Self::Error> {
        let release_name = self.labels.get(DEPLOY_ROBOT_TAG_MARKER_NAME);
        let release_version = self
            .labels
            .get(DEPLOY_ROBOT_TAG_MARKER_VERSION)
            .map(|item| item.parse::<u32>().ok())
            .flatten();
        if let Some(release_name) = release_name {
            if let Some(release_version) = release_version {
                let state: RobotMachineState = match self.status {
                    Status::Running => RobotMachineState::Started,
                    Status::Off => RobotMachineState::Stopped,
                    Status::Unknown => RobotMachineState::Error,
                    Status::Starting => RobotMachineState::Transitioning,
                    Status::Stopping => RobotMachineState::Transitioning,
                    Status::Rebuilding => RobotMachineState::Transitioning,
                    Status::Migrating => RobotMachineState::Transitioning,
                    Status::Initializing => RobotMachineState::Transitioning,
                    Status::Deleting => RobotMachineState::Transitioning,
                };

                return Ok(RobotMachine::new(
                    ProviderType::Hetzner,
                    self.id.to_string(),
                    self.name,
                    release_name.to_string(),
                    release_version,
                    None,
                    None,
                    state,
                    self.datacenter.location.id.to_string(),
                ));
            }
        }
        Err(RobotError::NotManagedByDeployRobot)
    }
}

pub async fn create_machine<S1: AsRef<str>, S2: Into<String>>(
    config: &hcloud::apis::configuration::Configuration,
    release_name: S1,
    release_version: u32,
    user_data: S2,
    os_id: u32,
    plan: &RobotPlan,
) -> Result<RobotMachine, RobotError> {
    let mut labels = HashMap::new();
    labels.insert(
        String::from(DEPLOY_ROBOT_TAG_MARKER_NAME),
        String::from(release_name.as_ref()),
    );
    labels.insert(
        String::from(DEPLOY_ROBOT_TAG_MARKER_VERSION),
        release_version.to_string(),
    );
    let create_server_request = CreateServerRequest {
        automount: None,
        datacenter: None,
        firewalls: None,
        image: os_id.to_string(),
        labels: Some(labels),
        location: None,
        name: release_name.as_ref().to_string(),
        networks: None,
        placement_group: None,
        public_net: None,
        server_type: plan.id().to_string(),
        ssh_keys: None,
        start_after_create: None,
        user_data: Some(user_data.into()),
        volumes: None,
    };
    let params = CreateServerParams {
        create_server_request: Some(create_server_request),
    };
    let server = *hcloud::apis::servers_api::create_server(config, params)
        .await?
        .server;
    Ok(server.try_into()?)
}
