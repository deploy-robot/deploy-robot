mod config;
mod robotlib;

use std::time::Duration;

use async_std::task;
use clap::Parser;
use config::Config;
use log::info;
use robotlib::RobotError;
use robotlib::RobotLib;

use crate::robotlib::RobotMachineState;
//use ssh_key::{rand_core::OsRng, Algorithm, PrivateKey};

#[async_std::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let args = Config::parse();

    /*
        let path = Path::new("/home/segler/.ssh/id_rsa");
        let current_key = PrivateKey::read_openssh_file(path)?;
        println!("C: {:#?}", current_key);

        let private_key = PrivateKey::random(
            &mut OsRng,
            Algorithm::Rsa {
                hash: Some(ssh_key::HashAlg::Sha512),
            },
        )?;
        let priv_key = current_key
        .to_openssh(ssh_key::LineEnding::CRLF)?
        .as_str()
        .to_string();
    println!("PRIV Key: {}", priv_key);
    let pub_key = current_key.public_key().to_openssh()?;
    println!("PUB Key: {}", pub_key);

    use ssh;

    let mut session = ssh::create_session()
    .username("root")
    .private_key(priv_key)
    .connect("at1.api.radio-browser.info:22")?
    .run_local();
    let exec = session.open_exec()?;
    let vec: Vec<u8> = exec.send_command("ls abcd")?;
    println!("{}", String::from_utf8(vec)?);
    */

    info!("Adding connections..");
    let mut robot = RobotLib::new();
    if let Some(key) = args.vultr_api_key {
        println!("🌩  Vultr added");
        robot.add_vultr(&key);
    }

    if let Some(key) = args.linode_api_key {
        println!("🌩  Linode added");
        robot.add_linode(&key);
    }

    if let Some(key) = args.digitalocean_access_token {
        println!("🌩  DigitalOcean added");
        robot.add_digitalocean(&key);
    }

    if let Some(key) = args.hetzner_api_token {
        println!("🌩  Hetzner added");
        robot.add_hetzner(&key);
    }

    if let Some(username) = args.upcloud_username {
        if let Some(password) = args.upcloud_password {
            println!("🌩  Upcloud added");
            robot.add_upcloud(&username, &password);
        }
    }

    if let Some(secret_key) = args.scaleway_api_secret {
        println!("🌩  Scaleway added");
        robot.add_scaleway(&secret_key);
    }

    match args.sub {
        config::CliCommand::ListDns {} => list_dns(robot).await?,
        config::CliCommand::ListOs {} => list_os(robot).await?,
        config::CliCommand::ListMachineTypes {
            min_ram,
            max_ram,
            min_cpu,
            max_cpu,
        } => {
            list_plans(
                robot,
                min_ram.map(|item| item.as_u64()),
                max_ram.map(|item| item.as_u64()),
                min_cpu,
                max_cpu,
            )
            .await?
        }
        config::CliCommand::ListMachines {} => list_machines(robot).await?,
        config::CliCommand::ListReleases {} => list_releases(robot).await?,
        config::CliCommand::CreateRelease {
            mem,
            cpus,
            release_name,
            image,
            container_port,
            hostname,
        } => {
            create_machine(
                robot,
                release_name,
                mem.map(|item| item.as_u64()),
                cpus,
                image,
                container_port,
                hostname,
                args.vultr_os_id,
                args.hetzner_os_id,
                args.upcloud_os_id,
                args.digitalocean_os_id,
                args.linode_os_id,
                args.scaleway_marketplace_os_id,
                args.scaleway_default_project_id,
            )
            .await?
        }
        config::CliCommand::DeleteRelease {
            release_name,
            hostname,
        } => delete_release(robot, &release_name, hostname).await?,
    };
    Ok(())
}

async fn list_os(robot: RobotLib) -> Result<(), RobotError> {
    let mut plans = robot.list_os().await?;
    plans.sort_unstable_by_key(|item| item.name().to_string());
    for plan in plans.iter() {
        println!(
            "{provider:<30} | {id:<30} | {name:<50}",
            provider = plan.provider().to_string(),
            id = plan.id(),
            name = plan.name(),
        );
    }
    Ok(())
}

async fn list_machines(robot: RobotLib) -> Result<(), RobotError> {
    let machines = robot.list_machines().await?;
    for machine in machines.iter() {
        println!("{:?}", machine);
    }
    Ok(())
}

async fn list_releases(robot: RobotLib) -> Result<(), RobotError> {
    let releases = robot.list_releases().await?;
    for release in releases.iter() {
        println!("{} {}", release.name(), release.version());
    }
    Ok(())
}

async fn list_plans(
    robot: RobotLib,
    min_ram: Option<u64>,
    max_ram: Option<u64>,
    min_cpu: Option<u32>,
    max_cpu: Option<u32>,
) -> Result<(), RobotError> {
    let plans = robot.list_plans(min_ram, max_ram, min_cpu, max_cpu).await;
    for plan in plans.iter() {
        println!(
            "{provider:<20} | {name:<30} | {cpus:>5} | {memory:>10} | {disk:>10} | {traffic:>10} | {location:<15} | {pricing:>10.2}",
            provider = plan.provider().to_string(),
            name = plan.id(),
            location = plan.location(),
            cpus = plan.cpus(),
            memory = plan.memory_human(),
            disk = plan.disk_human().unwrap_or(String::from("None")),
            traffic = plan.traffic_human().unwrap_or(String::from("None")),
            pricing = plan.monthly_pricing(),
        );
    }
    Ok(())
}

async fn create_machine<S1: AsRef<str>, S2: AsRef<str>, S3: AsRef<str>>(
    robot: RobotLib,
    release_name: S1,
    mem: Option<u64>,
    cpus: Option<u32>,
    image: S2,
    container_port: u32,
    hostname: Option<S3>,
    vultr_os_id: u32,
    hetzner_os_id: u32,
    upcloud_os_id: String,
    digitalocean_os_id: String,
    linode_os_id: String,
    scaleway_marketplace_os_id: String,
    scaleway_default_project_id: Option<String>,
) -> Result<(), RobotError> {
    println!(
        "Searching for old releases with name: {}",
        release_name.as_ref()
    );
    let releases = robot.get_releases_by_name(release_name.as_ref()).await?;
    let max_release_version = releases
        .iter()
        .fold(0, |acc, item| std::cmp::max(acc, item.version()));
    if releases.len() > 0 {
        println!(
            "Old release found, updating release {}..",
            max_release_version
        );
    } else {
        println!("No release found, initial deploy..");
    }
    let plans = robot.list_plans(mem, None, cpus, None).await;
    let plan = &plans[0];
    println!(
        "Deploy plan: provider={} cpus={} mem={}",
        plan.provider(),
        plan.cpus(),
        plan.memory()
    );
    let machine = robot
        .create_machine(
            &plan,
            release_name,
            max_release_version + 1,
            image,
            container_port,
            vultr_os_id,
            hetzner_os_id,
            upcloud_os_id,
            digitalocean_os_id,
            linode_os_id,
            scaleway_marketplace_os_id,
            scaleway_default_project_id,
        )
        .await?;
    println!("Deploy done");
    let mut new_ipv4 = String::new();
    loop {
        println!("Waiting for machine to have IPv4..");
        let machine = robot.get_machine(&machine).await?;
        // check if ip is available
        // TRUE -> set host and break
        if let Some(ipv4) = machine.ipv4 {
            if !ipv4.eq("0.0.0.0") && !ipv4.is_empty() {
                new_ipv4 = ipv4.clone();
                println!("Got IPv4={}", ipv4);
                break;
            }
        }
        // FALSE -> wait 1 sec
        task::sleep(Duration::from_secs(1)).await;
    }
    loop {
        println!("Waiting for machine to be running..");
        let machine = robot.get_machine(&machine).await?;
        if machine.state().eq(&RobotMachineState::Started) {
            println!("Machine is running");
            break;
        }
        if machine.state().eq(&RobotMachineState::Error) {
            println!("Machine is in error state");
            std::process::exit(1);
        }
        task::sleep(Duration::from_secs(1)).await;
    }
    let url = format!("http://{}", new_ipv4);
    loop {
        println!("Wait for open port on new container..");
        let result = reqwest::get(&url).await;
        if result.is_ok() {
            break;
        }
        task::sleep(Duration::from_secs(1)).await;
    }
    if let Some(hostname) = hostname {
        println!("Searching old DNS records for '{}'", hostname.as_ref());
        if let Some((entryname, domainname)) = hostname.as_ref().split_once(".") {
            let domains = robot.get_dns_domain(domainname).await?;
            for domain in domains {
                let mut found_record = false;
                println!("Found domain @ {}", domain.provider);
                let records = domain.get_records().await?;
                let mut max_ttl = 0;
                for mut record in records {
                    if record.entryname.eq(entryname) {
                        record.update_data(&new_ipv4).await?;
                        found_record = true;
                        println!("Found dns record with TTL={}", record.ttl);
                        max_ttl = std::cmp::max(max_ttl, record.ttl);
                    }
                }
                if max_ttl > 0 {
                    println!(
                        "Waiting for TTL ({}) secs before deleting old release",
                        max_ttl
                    );
                    task::sleep(Duration::from_secs(max_ttl)).await;
                }
                if !found_record {
                    println!("Create new dns record {} @ {}", entryname, domain.provider);
                    domain.create_record("A", entryname, &new_ipv4, 300).await?;
                }
            }
        }
    }
    for old_release in releases {
        println!("Deleting old release {}", old_release.version());
        robot
            .delete_release(old_release.name(), old_release.version())
            .await?;
    }
    Ok(())
}

async fn delete_release<S1: AsRef<str>, S2: AsRef<str>>(
    robot: RobotLib,
    release_name: S1,
    hostname: Option<S2>,
) -> Result<(), RobotError> {
    println!("Searching releases by name '{}'", release_name.as_ref());
    let releases = robot.get_releases_by_name(release_name.as_ref()).await?;
    for old_release in releases {
        println!("Deleting release version {}", old_release.version(),);
        for machine in old_release.machines() {
            println!(
                "- Deleting machine {} @ {}",
                machine.id(),
                machine.provider()
            );
            robot.delete_machine(machine).await?;
        }
    }
    if let Some(hostname) = hostname {
        println!("Delete dns name '{}'", hostname.as_ref());
        robot.delete_dns_name(hostname.as_ref()).await?;
    }
    Ok(())
}

async fn list_dns(robot: RobotLib) -> Result<(), RobotError> {
    let list = robot.list_dns().await?;
    println!("{:#?}", list);
    Ok(())
}
