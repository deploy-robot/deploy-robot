use byte_unit::Byte;
use clap::{Parser, Subcommand};

/// DEPLOY-ROBOT is a program to simply deploy things
/// the cheapest way possible on all supported clouds.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(arg_required_else_help(true))]
pub struct Config {
    /// API key for vultr.com
    #[arg(long, env, global = true)]
    pub vultr_api_key: Option<String>,

    /// Vultr os image id
    #[arg(long, default_value_t = 2104)]
    pub vultr_os_id: u32,

    /// API key for linode.com
    #[arg(long, env, global = true)]
    pub linode_api_key: Option<String>,

    /// Linode OS image
    #[arg(long, env, global = true, default_value = "linode/ubuntu22.04")]
    pub linode_os_id: String,

    /// API key for digitalocean.com
    #[arg(long, env, global = true)]
    pub digitalocean_access_token: Option<String>,

    /// DigitalOcean OS image
    #[arg(long, env, global = true, default_value = "112929454")]
    pub digitalocean_os_id: String,

    /*
    /// API key for exoscale.com
    #[arg(long, env, global = true)]
    pub exoscale_api_key: Option<String>,

    /// API secret for exoscale.com
    #[arg(long, env, global = true)]
    pub exoscale_api_secret: Option<String>,
    */
    /// API secret for scaleway.com
    #[arg(long, env = "SCW_SECRET_KEY", global = true)]
    pub scaleway_api_secret: Option<String>,

    /// Scaleway marketplace OS id
    #[arg(
        long,
        global = true,
        default_value = "1123148c-7660-4cb2-9fd3-7b5b4896f72f"
    )]
    pub scaleway_marketplace_os_id: String,

    /// Default project ID for scaleway.com
    #[arg(long, env = "SCW_DEFAULT_PROJECT_ID", global = true)]
    pub scaleway_default_project_id: Option<String>,

    /// API key for hetzner.com
    #[arg(long, env, global = true)]
    pub hetzner_api_token: Option<String>,

    /// Hetzner os image id
    #[arg(long, env, default_value_t = 67794396)]
    pub hetzner_os_id: u32,

    /// Username for upcloud.com
    #[arg(long, env, global = true)]
    pub upcloud_username: Option<String>,

    /// Password for upcloud.com
    #[arg(long, env, global = true)]
    pub upcloud_password: Option<String>,

    /// Upcloud OS image
    #[arg(
        long,
        env,
        global = true,
        default_value = "01000000-0000-4000-8000-000030220200"
    )]
    pub upcloud_os_id: String,

    /// What to do
    #[clap(subcommand)]
    pub sub: CliCommand,
}

#[derive(Debug, Subcommand)]

pub enum CliCommand {
    /// List all supported OSs with the IDs
    ListOs {},
    /// List all supported machine types
    ListMachineTypes {
        /// Min memory with postfix (KB,MB,GB,TB,KiB,MiB,TiB)
        #[arg(long)]
        min_ram: Option<Byte>,
        /// Max memory with postfix (KB,MB,GB,TB,KiB,MiB,TiB)
        #[arg(long)]
        max_ram: Option<Byte>,
        /// Min amount of cpu threads
        #[arg(long)]
        min_cpu: Option<u32>,
        /// Max amount of cpu threads
        #[arg(long)]
        max_cpu: Option<u32>,
    },
    /// List machines that are managed by deploy-robot
    ListMachines {},
    /// List releases accross all enabled providers
    ListReleases {},
    /// Create a release and choose the cheapest machine accross the providers
    CreateRelease {
        /// Memory with postfix (KB,MB,GB,TB,KiB,MiB,TiB)
        #[arg(long)]
        mem: Option<Byte>,
        /// Amount of cpu threads
        #[arg(long)]
        cpus: Option<u32>,
        /// Name for the release
        release_name: String,
        /// Docker image to run on the machine
        #[arg(long, default_value = "nginx:latest")]
        image: String,
        /// Listening port of the container
        #[arg(long, default_value_t = 80)]
        container_port: u32,
        /// DNS domain name
        #[arg(long, env = "HOSTNAME")]
        hostname: Option<String>,
    },
    /// Delete all by deploy-robot managed assets of the release
    DeleteRelease {
        /// Name for the release
        release_name: String,
        /// DNS domain name
        #[arg(long, env = "HOSTNAME")]
        hostname: Option<String>,
    },
    ListDns {},
}
