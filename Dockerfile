FROM alpine:edge as builder
WORKDIR /usr/src/deploy-robot
COPY . .
RUN apk add rust cargo pkgconfig openssl-dev gcc musl-dev
RUN cargo build --release

FROM alpine:edge
RUN apk add libgcc
COPY --from=builder /usr/src/deploy-robot/target/release/deploy-robot /usr/bin/deploy-robot
CMD [ "deploy-robot" ]