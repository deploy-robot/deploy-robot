# deploy-robot

## ATTENTION

This project is still in heavy development, PLEASE USE AT YOUR OWN RISK!

## Use cases

These use cases are NOT YET WORKING fully!!! most of it is still in planing phase.

* You want to deploy your container to a virtual machine from one of the cloud providers in a simple way.
* Upgrading the container should be as simple as just calling the same script on initial deploy again.
* If you provide login credentials for multiple different providers, it will automatically choose the cheapest one.
* You can specify the amount of cpu and ram needed, it will automatically choose the cheapest option to run your workload.
* If another option gets cheaper on a future deploy, it will migrate the container to it.
* It should work from within popular programming environments (gitlab, github) in a simple way
* It should support many different cloud providers, big AND small. Simple to add new cloud providers.
* Self contained deployment without the need for state files (NOT like terraform), support for automatic cleanup as well (like HELM).
* Have sensible defaults
* Environment variable friendly
* Provide machine metrics
* Support for autoscaling (UP+DOWN) in a simple automatic way (cpu load based)
* Zero downtime deployments
* Automatic DNS update for supported DNS providers
* Automatic HTTPS connections via let's encrypt
* Single binary installation with no dependencies or usage via minimal docker container

## What is already working?

* Creation of instances AND start of a test nginx container (uses cloud-init)
* List of deployed instances
* Deletion of instances

## Currently supported cloud providers

### Instances

* Scaleway
* Vultr
* Digitalocean
* Linode
* Hetzner
* Upcloud

### DNS

* Vultr

## How to use it?

There is a docker build for the latest version. You can easily use it like this:

```shell
docker run registry.gitlab.com/deploy-robot/deploy-robot:latest
```
